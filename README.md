# Draw Pictures with Code
## (and make them move)

A digital zine / simple website created to document an online workshop I gave with [Solarpunk Magic Computer Club](https://solarpunk.cool/magic/computer/club/).

### Read the published [Draw with Code zine](https://solarpunk.cool/zines/draw-with-code/index.html)
![The words 'DRAW WITH CODE' (formed by simple overlapping squares, circles and rectangles) are animated onto and then off the screen](draw-with-code.svg)
